from flask_mail import Message
from src.flask_app import mail


class EmailHandler(object):

    def send_email(self, subject, sender, recipients, body_text='', html_text=''):
        msg = Message(subject,
                      sender=sender,
                      recipients=recipients)

        msg.body = body_text
        msg.html = html_text
        mail.send(msg)