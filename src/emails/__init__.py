from flask import Blueprint

emails = Blueprint('emails', __name__, template_folder='templates', static_folder='static',
                   static_url_path='/emails/static')
