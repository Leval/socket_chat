from wtforms import Form, TextField, PasswordField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Required, Length, EqualTo, ValidationError
from src.users.UserFabric import UserFabric

user_fabric = UserFabric()


class SignInForm(Form):
    email = TextField('email', validators=[Required(message="Email is required"), Length(max=25)], description={'placeholder': 'Email'})
    password = PasswordField('password',
                             validators=[Required(message="Password is required"),
                                         Length(min=4, max=25,
                                                message='Password must contain minimum 4 and maximum 25 symbols')])

    def validate_password(form, field):
        user = user_fabric.get_user_by_email_password(dict(email=form.email.data, password=form.password.data))
        if not user:
            raise ValidationError("There is no such user. Check entered username and password")


class SignUpForm(Form):
    username = TextField('username', validators=[Length(max=25, message='Username must contain less then 25 symbols')],
                         description={'placeholder': 'Username'})
    email = TextField('email', validators=[Required(message="Email is required"), Length(max=25)],
                      description={'placeholder': 'Email'})
    password = PasswordField('password',
                             validators=[Required(message="Password is required"),
                                         EqualTo('password_confirm', message='Passwords must match'),
                                         Length(min=4, max=25,
                                                message='Password must contain minimum 4 and maximum 25 symbols')])
    password_confirm = PasswordField('password_confirm',
                                     validators=[Required(message="Password confirmation is required"),
                                                 Length(min=4, max=25)])


class EmailForm(Form):
    email = EmailField('email', validators=[Required(message="Email is required")])

    def validate_email(form, field):
        user = user_fabric.get_item_by_filters(dict(email=form.email.data))
        if not user:
            raise ValidationError("There is no user with such email. Please, check input")


class ResetPWForm(Form):
    password = PasswordField('password',
                             validators=[Required(message="Password is required"),
                                         EqualTo('password_confirm', message='Passwords must match'),
                                         Length(min=4, max=25,
                                                message='Password must contain minimum 4 and maximum 25 symbols')])
    password_confirm = PasswordField('password_confirm',
                                     validators=[Required(message="Password confirmation is required"),
                                                 Length(min=4, max=25)])

    def validate_password_confirm(form, field):
        if form.password.data != form.password_confirm.data:
            raise ValidationError("Passwords don't match")
