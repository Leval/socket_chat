SocketChat = window.SocketChat || {};
SocketChat.data = SocketChat.data || {};

SocketChat.data.users = {
    init: function () {
        this.initSignUpForm();
        this.initSignInForm();
        this.initSubmitSResetPWForm();
    },

    initSignUpForm: function () {
        var self = this;
        $('.sign-up-button').on('click', function () {
            $.ajax({
                url: '/signup',
                success: function (data) {
                    $('.block-for-form').html(data);
                    self.initSubmitSignUpForm();
                }
            });
        });
    },
    initSubmitSignUpForm: function () {
        var self = this;
        $('#signup-button').on('click', function () {
            self.initProcessSignUpForm();
        });

        $('#username').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessSignUpForm();
            }
        });

        $('#password').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessSignUpForm();
            }
        });

        $('#password_confirm').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessSubmitSignInForm();
            }
        });
    },
    initProcessSignUpForm: function () {
        var self = this;
        $.ajax({
            url: '/signup',
            data: {
                username: $('#username').val(),
                email: $('#email').val(),
                password: $('#password').val(),
                password_confirm: $('#password_confirm').val()
            },
            type: 'post',
            success: function (data) {
                if (data.result) {
                    location = '/rooms';
                }
                else {
                    $('.block-for-form').html(data);
                    self.initSubmitSignUpForm();
                }
            }
        });
    },

    initEmailForm: function () {
        var self = this;
        $('.forgot-pw-button').on('click', function () {
            $.ajax({
                url: '/reset/email',
                success: function (data) {
                    $('.block-for-form').html(data);
                    self.initSubmitEmailForm();
                }
            });
        });
    },

    initSubmitEmailForm: function () {
        var self = this;
        $('#reset-email-button').on('click', function () {
            self.initProcessSubmitEmailForm();
        });

        $('#email').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessSubmitEmailForm();
            }
        });
    },

    initProcessSubmitEmailForm: function () {
        $.ajax({
            url: '/reset/email',
            data: {email: $('#email').val()},
            type: "post",
            success: function (data) {
                if (data.success == true) {
                    $('.block-for-form').html("<span>Letter with further instructions was sent to your email</span>");
                }
                else
                {
                    $('.block-for-form').html(data);
                    self.initSubmitEmailForm();
                }
            }
        });
    },

    initSubmitSResetPWForm: function () {
        var self = this;
        $('#reset-pw-button').on('click', function () {
            self.initProcessResetPWForm();
        });

        $('#password').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessResetPWForm();
            }
        });

        $('#password_confirm').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessResetPWForm();
            }
        });
    },

    initProcessResetPWForm: function () {
        var self = this;
        $.ajax({
            url: '/reset/' + $('#reset-pw-button').data("hash"),
            data: {
                password: $('#password').val(),
                password_confirm: $('#password_confirm').val()
            },
            type: 'post',
            success: function (data) {
                if (data.success) {
                    location = '/rooms';
                }
                else {
                    $('.block-for-form').html(data);
                    self.initSubmitSResetPWForm();
                }
            }
        });
    },

    initSignInForm: function () {
        var self = this;
        $('.sign-in-button').on('click', function () {
            $.ajax({
                url: '/signin',
                success: function (data) {
                    $('.block-for-form').html(data);
                    self.initEmailForm();
                    self.initSubmitSignInForm();
                }
            });
        });
    },

    initSubmitSignInForm: function () {
        var self = this;
        $('#username').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessSubmitSignInForm();
            }
        });

        $('#password').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessSubmitSignInForm();
            }
        });

        $('#signin-button').on('click', function () {
            self.initProcessSubmitSignInForm();
        });
    },

    initProcessSubmitSignInForm: function () {
        var self = this;
        $.ajax({
            url: '/signin',
            data: {email: $('#email').val(), password: $('#password').val()},
            type: 'post',
            success: function (data) {
                if (data.result) {
                    location = '/rooms';
                }
                else {
                    $('.block-for-form').html(data);
                    self.initSubmitSignInForm();
                }
            }
        });
    }
};

$(function () {
    SocketChat.data.users.init();
});