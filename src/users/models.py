from flask.ext.login import AnonymousUserMixin
from src.base.BaseModel import BaseModel
from src.flask_app import db
from src.rooms.models import Room

rooms = db.Table('users_rooms',
                 db.Column('room_id', db.Integer, db.ForeignKey('rooms.id')),
                 db.Column('user_id', db.Integer, db.ForeignKey('users.id'))
                 )


class User(BaseModel):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64))
    email = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(256))
    rooms = db.relationship(Room, secondary=rooms,
                        lazy='dynamic', primaryjoin=(rooms.c.user_id == id),
                        secondaryjoin=(rooms.c.room_id == Room.id),
                        backref=db.backref('users', lazy='dynamic'))

    def in_room(self, room):
        if room in self.rooms:
            return True
        return False

    def add_room(self, room):
        if not self.in_room(room):
            self.rooms.append(room)
            db.session.commit()
            return self

    def delete_room(self, room):
        if self.in_room(room):
            self.rooms.remove(room)
            db.session.commit()
            return self

    def is_authenticated(self):
        if isinstance(self, AnonymousUserMixin):
            return False
        return True

    def is_anonymous(self):
        if isinstance(self, AnonymousUserMixin):
            return True
        return False

    def is_active(self):
        return True

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return 'User %r, rooms: %r' % (self.username, ', '.join(room.title for room in self.rooms))

db.create_all()
db.session.commit()
