from flask import session
from flask.ext.login import logout_user, AnonymousUserMixin
from passlib.handlers.pbkdf2 import pbkdf2_sha256
from src.base.BaseFabric import BaseFabric
from src.flask_app import db
from src.users.models import User


class UserFabric(BaseFabric):
    model_handler = User

    def get_user_by_email_password(self, data):
        user = self.get_item_by_filters({"email": data.get("email", None)})
        if pbkdf2_sha256.verify(data.get("password", None), user.password):
            return user
        return None

    def create_user(self, data):
        if data.get('password', None) == data.get('password_confirm', None):
            pw_hash = self.make_password_hash(data.get('password', None))
            return self.create(dict(username=data.get('username', None),
                                    email=data.get('email', None),
                                    password=pw_hash))

    def reset_password(self, user, password):
        user.password = self.make_password_hash(password)
        return self.update(user)

    @staticmethod
    def make_password_hash(password):
        return pbkdf2_sha256.encrypt(password, rounds=200000, salt_size=16)