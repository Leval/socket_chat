import base64
import json
from flask import render_template, Blueprint, request, Response, url_for
from flask.ext.login import login_required, logout_user, redirect, login_user, current_user
from src.config import Config
from src.emails.EmailHandler import EmailHandler
from send_email_tasks import send_email
from src.users.forms import SignInForm, SignUpForm, EmailForm, ResetPWForm
from src.users.UserFabric import UserFabric

users = Blueprint('users', __name__, template_folder='templates', static_folder='static',
                  static_url_path='/users/static')
user_fabric = UserFabric()
email_handler = EmailHandler()
ADMINS = Config().ADMINS


@users.route('/')
def index():
    if current_user.username != 'Guest':
        return redirect('/rooms')
    return render_template('index.html')


@users.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SignInForm(request.form)
    if request.method == 'POST' and form.validate():
        user = user_fabric.get_user_by_email_password(form.data)
        if user:
            login_user(user)
            return Response(json.dumps({'result': True}), content_type='application/json')
    return render_template('signin.html', form=form)


@users.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUpForm(request.form)
    if request.method == 'POST' and form.validate():
        user = user_fabric.create_user(form.data)
        if user:
            login_user(user)
        return Response(json.dumps({'result': True}), content_type='application/json')
    return render_template('signup.html', form=form)


@users.route('/reset/email', methods=['GET', 'POST'])
def reset_email():
    form = EmailForm(request.form)
    if request.method == 'POST' and form.validate():
        user = user_fabric.get_item_by_filters({"email": form.data.get('email', None)})
        url = request.url_root[:-1] + url_for('users.reset',
                                              email_hash=base64.encodestring(form.data.get('email', None)))
        # email_handler.send_email("MiMiChat! | Reset password", ADMINS[0], [form.data.get('email', None)],
        #                          html_text=render_template('reset_password.html', url=url, username=user.username))
        send_email.delay("MiMiChat! | Reset password", ADMINS[0], form.data.get('email', None),
                   html_text=render_template('reset_password.html', url=url, username=user.username))
        return Response(json.dumps(dict(success=True)), content_type='application/json')
    return render_template('reset_email.html', form=form)


@users.route('/reset/<email_hash>', methods=['GET', 'POST'])
def reset(email_hash):
    email = base64.decodestring(email_hash)
    user = user_fabric.get_item_by_filters({"email": email})
    form = ResetPWForm(request.form)
    if request.method == 'POST' and form.validate():
        user_fabric.reset_password(user, form.data.get("password", None))
        login_user(user)
        return Response(json.dumps(dict(success=True)), content_type='application/json')
    return render_template('reset_pw.html', form=form, email_hash=email_hash)


@users.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect('/')
