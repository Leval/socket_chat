import datetime
from src.flask_app import db


class BaseModel(db.Model):
    __abstract__ = True
    created_date = db.Column(db.DateTime(timezone=True), default=datetime.datetime.utcnow())
    updated_date = db.Column(db.DateTime, onupdate=datetime.datetime.utcnow())
    is_deleted = db.Column(db.Boolean, default=False)