from src.flask_app import db


class BaseFabric(object):
    model_handler = None

    def get_all(self):
        return self.model_handler.query.all()

    def get_by_id(self, id):
        return self.model_handler.query.get(id)

    def get_item_by_filters(self, query_data):
        return self.model_handler.query.filter_by(**query_data).first()

    def get_by_filters(self, query_data):
        return self.model_handler.query.filter_by(**query_data)

    def create(self, data):
        item = self.model_handler(**data)

        db.session.add(item)
        db.session.commit()
        return item

    def update(self, item):
        db.session.add(item)
        db.session.commit()
        return item