from flask import Flask, request, g
from flask.ext.login import LoginManager
from flask.ext.mail import Mail
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.script import Manager
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)

APP_SETTINGS = 'src.config.DevConfig'

app.config.from_object(APP_SETTINGS)
db = SQLAlchemy(app)
mail = Mail(app)
# from users import users
# from rooms import rooms
# from messages import messages
# from emails import emails
#
# app.register_blueprint(users)
# app.register_blueprint(rooms)
# app.register_blueprint(messages)
# app.register_blueprint(emails)

# login_manager = LoginManager()
# login_manager.init_app(app)


# migrate = Migrate(app, db)
# manager = Manager(app)

# manager.add_command('db', MigrateCommand)