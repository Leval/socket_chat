import os
import smtplib


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'secret_key'

    basedir = os.path.abspath(os.path.dirname(__file__))
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'chat.db')
    DATABASE_USER = "mimichat"
    DATABASE_PASSWORD = "mimichat123"
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@localhost:5432/mimichat'.format(DATABASE_USER, DATABASE_PASSWORD)

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'mimichat.noreply@gmail.com'
    MAIL_PASSWORD = 'mimichatnoreply'

    server = smtplib.SMTP(MAIL_SERVER, MAIL_PORT)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(MAIL_USERNAME, MAIL_PASSWORD)
    # administrator list
    ADMINS = ['mimichat.noreply@gmail.com']


class DevConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class ProdConfig(Config):
    DEBUG = False