from src.base.BaseModel import BaseModel
from src.flask_app import db
from src.rooms.models import Room
from src.users.models import User


class Message(BaseModel):
    __messages__ = 'message'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(1024))
    room = db.relationship(Room, backref='messages')
    user = db.relationship(User, backref='messages')
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'))

    def __repr__(self):
        return 'Message id: %r, room: %r' % (unicode(self.id), self.room_id)

db.create_all()
db.session.commit()