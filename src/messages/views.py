import json
from flask import Blueprint, request, Response
from src.messages.MessagesFabric import MessagesFabric
from src.messages.MessagesForm import MessageForm
from src.rooms import RoomsFabric

messages = Blueprint('messages', __name__, template_folder='templates', static_folder='static', static_url_path='/messages/static')
messages_fabric = MessagesFabric()
rooms_fabric = RoomsFabric()


@messages.route('/message', methods=['POST'])
def save_message():
    form = MessageForm(request.form)
    if request.method == "POST" and form.validate():
        room = rooms_fabric.get_by_id(form.data.get('room_id'))
        form.data['room'] = room
        messages_fabric.create(form.data)
    return Response(json.dumps(dict(success='success')), content_type="application/json")
