from wtforms import Form, TextField
from wtforms.validators import Required, Length


class MessageForm(Form):
    user_id = TextField('user_id', validators=[Required()])
    room_id = TextField('room_id', validators=[Required()])
    text = TextField('text', validators=[Required(), Length(max=1023)])