SocketChat = window.SocketChat || {};
SocketChat.data = SocketChat.data || {};

SocketChat.data.rooms = {
    user_id: '',
    user_username: '',
    room_id: '',
    room_ids: '',
    room_static_url: '',
    invite_email: '',

    init: function () {
        this.initWebSocket();
        this.initOpenInitialRoom();
        this.initOpenRoom();
        this.initCreateRoom();
        this.initAddSmile();
        this.initShowSmilesBlock();
        this.initInviteToRoom();
    },
    initOpenInitialRoom: function () {
        var array = location.hash.split('#');
        var room = array[array.length - 1];
        if (room) {
            $('.rooms-index-block').hide();
            $('.room-item-block[data-room="' + room + '"]').addClass('active');
            $('.main-chat-block[data-room="' + room + '"]').css('display', 'inline-block');
        }
    },

    initWebSocket: function () {
        var self = this;
        var messageContainer = $("#messages");
        try {
            if ("WebSocket" in window && $(".main-chat-block").length > 0) {
                var messages = $("#messages");
                messages.scrollTop(messages[0].scrollHeight);
                var host;
                if (location.hostname.indexOf('petsterr') > 0) {
                    host = "ws://mimichatws.petsterr.com/socket/"
                } else {
                    host = "ws://" + location.hostname + ":9000/socket/";
                }
                var ws = new WebSocket(host);
                ws.onopen = function (evt) {
                    var msg = {
                        type: "rooms",
                        text: JSON.parse(self.room_ids),
                        id: self.user_id,
                        username: self.user_username,
                        date: Date.now()
                    };
                    ws.send(JSON.stringify(msg));
                    ws.send(JSON.stringify({type: "members", text: JSON.parse(self.room_ids)}));
                };

                $('.chat-room-block').on('click', '#send_message', function () {
                    var room = $(this).data('room');
                    self.initSendMessage(ws, room);
                    $('.enter-text-block[data-room="' + room + '"]').focus();
                });

                $(document.activeElement).keypress(function (e) {

                    var room = $(document.activeElement).data('room');
                    if (e.which == 13) {
                        e.preventDefault();
                        if ($('#id_text[data-room="' + room + '"]').html().length > 0) {
                            self.initSendMessage(ws, room);
                        }
                    }
                    else {
                        ws.send(JSON.stringify({type: "typing", room: room}));
                    }
                });
                setInterval(function () {
                    if (ws.readyState === 1) {
                        try
                        {
                            ws.send(JSON.stringify({type: "ping"}))
                        }
                        catch (err) {
                            ws.close();
                        }
                    } else {
                        ws.close();
                    }
                    //try {
                    //    ws.send(JSON.stringify({type: "ping"}));
                    //}
                    //catch (err) {
                    //    console.log(err);
                    //}
                }, 120000);

                //ws.onerror = function (evt) {
                //    ws.close();
                //};
                ws.onmessage = function (evt) {
                    var received_msg = JSON.parse(evt.data);
                    if (received_msg.type == 'message') {
                        $('#messages[data-room="' + received_msg.room + '"]').append(
                            '<div class="message-item">' +
                            '<div class="message-title-block">' +
                            '<span class="message-user-block">' + received_msg.username + '</span> | ' +
                            '<span class="message-time-block">' + received_msg.datetime + '</span>' +
                            '</div>' +
                            '<div class="message-content-block">' + received_msg.text + '</div>' +
                            '</div>').scrollTop($('#messages[data-room="' + received_msg.room + '"]')[0].scrollHeight);
                        if (self.user_username != received_msg.username) {
                            $('link[rel="icon"]').attr('href', self.room_static_url + 'img/paw-red.png');
                            if ($('.unread[data-room="' + received_msg.room + '"]').length <= 0) {
                                $('.room-item-block[data-room="' + received_msg.room + '"]').append(
                                    '<span class="unread" data-room="' + received_msg.room + '">!</span>');
                            }
                        }
                        $(window).focus(function () {
                            $('link[rel="icon"]').attr('href', self.room_static_url + 'img/paw.png');
                            $('.room-item-block.active').find('.unread').remove();
                        });
                    }
                    else if (received_msg.type == 'rooms') {
                        $('.messages').each(function () {
                            $(this).append('<div class="message-item system-messages">' + received_msg.text + '</div>').scrollTop($("#messages")[0].scrollHeight);
                        });
                    }
                    else if (received_msg.type == 'onclose') {
                        $('.messages').each(function () {
                            $(this).append('<div class="message-item">' + received_msg.text + '</div>').scrollTop($("#messages")[0].scrollHeight);
                        });
                        var members_html = '';
                        $.each(received_msg.members, function (index, user) {
                            members_html += '<div class="member-item" data-user=' + user.id + '>' + user.username + '</div>';
                        });
                        $('.members-block[data-room="' + received_msg.room + '"').html(members_html);
                    }
                    else if (received_msg.type == 'members') {
                        var members_html = '';
                        $.each(received_msg.members, function (index, user) {
                            members_html += '<div class="member-item" data-user=' + user.id + '>' + user.username + '</div>';
                        });
                        $('.members-block[data-room="' + received_msg.room + '"').html(members_html);
                    }
                    else if (received_msg.type == 'typing') {
                        var typing_block = $('.members-block[data-room="' + received_msg.room + '"] .member-item[data-user="' + received_msg.id + '"] .member-typing[data-user="' + received_msg.id + '"');
                        if (typing_block.length == 0) {
                            $('.members-block[data-room="' + received_msg.room + '"] .member-item[data-user="' + received_msg.id + '"]').append('<i class="member-typing" data-user="' + received_msg.id + '">...</i>');
                            setTimeout(
                                function () {
                                    $('.members-block[data-room="' + received_msg.room + '"] .member-item[data-user="' + received_msg.id + '"] .member-typing[data-user="' + received_msg.id + '"').remove();
                                },
                                2000);
                        }

                    }
                };
                ws.onclose = function () {
                    $('.messages').each(function () {
                        $(this).append('<div class="message-item system-messages">Connection is closed...</div>').scrollTop($("#messages")[0].scrollHeight);
                    });
                    //alert("connection closed");
                    //self.initWebSocket();

                };
            }
            else {
                messageContainer.append('<div class="message-item system-messages">WebSocket NOT supported by your Browser!</div>');
            }
        }
        catch (err) {
            console.log(err);
        }
    },

    initSendMessage: function (ws, room) {
        var self = this;
        var userDate = new Date();
        var tzOffset = userDate.getTimezoneOffset();
        var msg = {
            type: "message",
            text: $('#id_text[data-room="' + room + '"]').html(),
            id: self.user_id,
            username: self.user_username,
            room_id: room,
            tzoffset: tzOffset,
            date: Date.now()
        };
        ws.send(JSON.stringify(msg));
        $('#id_text[data-room="' + room + '"]').html('');
        $('#messages[data-room="' + room + '"]').scrollTop($('#messages[data-room="' + room + '"]')[0].scrollHeight);
    },

    initOpenRoom: function () {
        var self = this;
        $('.room-item-block').on('click', function () {
            var room = $(this).data('room');
            var messages = $('.messages[data-room="' + room + '"]');
            messages.scrollTop($('#messages[data-room="' + room + '"]')[0].scrollHeight);
            $('.unread[data-room="' + room + '"]').remove();
            $('.rooms-index-block').hide();
            $('.main-chat-block').hide();
            $('.room-item-block').removeClass('active');
            $(this).addClass('active');
            $('.main-chat-block[data-room="' + room + '"]').css('display', 'inline-block');
        });
    },
    initCreateRoom: function () {
        var self = this;
        $('.create-room-button').on('click', function () {
            self.initProcessCreateRoom();
        });

        $('#title').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.initProcessCreateRoom()
            }
        });
    },

    initProcessCreateRoom: function () {
        $.ajax({
            url: '/rooms',
            data: {title: $('#title').val(), is_private: $('#is_private').val()},
            type: 'post',
            success: function (data) {
                if (data.success) {
                    window.location = '/rooms#' + data.room;
                    location.reload();
                }
                else {
                    location.reload();
                }
            }
        });
    },

    initAddSmile: function () {
        var self = this;
        $('.smile-row img').on('click', function () {
            var room = $(this).data('room');
            $('#id_text[data-room="' + room + '"]').append(
                '<img data-smile="(1)" class="text-smile" src="' +
                self.room_static_url + 'img/emotions/64x64/64 ' +
                $(this).data('smile') + '.png"/>');
            $('.message-smiles-block[data-room="' + room + '"]').hide();
            $('#id_text[data-room="' + room + '"]').focus();
            var node = $('#id_text[data-room="' + room + '"]').get(0);
            if (document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
            {
                range = document.createRange();//Create a range (a range is a like the selection but invisible)
                range.selectNodeContents(node);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                selection = window.getSelection();//get the selection object (allows you to change selection)
                selection.removeAllRanges();//remove any selections already made
                selection.addRange(range);//make the range you have just created the visible selection
            }
            else if (document.selection)//IE 8 and lower
            {
                range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
                range.moveToElementText(node);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                range.select();//Select the range (make it the visible selection
            }
        });
    },

    initShowSmilesBlock: function () {
        var self = this;
        $('.chat-room-block').on('click', '#open-smiles', function () {
            var room = $(this).data('room');
            $('.message-smiles-block[data-room="' + room + '"]').fadeToggle();
        });
    },

    initInviteToRoom: function () {
        var self = this;
        $('.invite-button').on('click', function () {
            var room = $(this).data('room');
            $('.invite-block').fadeToggle();

            $('.invite-send-button').on('click', function () {
                $.ajax({
                    url: self.invite_email,
                    data: {room: room, email: $('#email[data-room="' + room + '"]').val()},
                    type: "post",
                    success: function (data) {
                        if (data.success) {
                            $('#email[data-room="' + room + '"]').val('');
                            $('.invite-block').hide();
                        }
                        else {
                            $('.errors[data-room="' + room + '"]').html('');
                            $.each(data.errors, function (key, value) {
                                $.each(value, function (index, error) {
                                    $('.errors[data-room="' + room + '"]').append('<p>' + error + '</p>');
                                });
                            });
                        }
                    }
                });
            });
        });
    }
};

$(function () {
    SocketChat.data.rooms.init();
});