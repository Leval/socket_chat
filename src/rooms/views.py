import json
import datetime

from flask import Blueprint, render_template, request, redirect, Response
from flask.ext.login import current_user, login_required
from send_email_tasks import send_email

from src.config import Config
from src.emails.EmailHandler import EmailHandler
from src.rooms.RoomsFabric import RoomsFabric
from src.rooms.RoomsForm import CreateRoomForm, EmailRoomForm

rooms = Blueprint('rooms', __name__, template_folder='templates', static_folder='static',
                  static_url_path='/rooms/static')
rooms_fabric = RoomsFabric()
email_handler = EmailHandler()
default_room_title = 'Room'
ADMINS = Config().ADMINS


@rooms.route('/rooms', methods=['GET', 'POST'])
@login_required
def room_index():
    rooms = rooms_fabric.get_public_rooms()
    form = CreateRoomForm(request.form)
    if request.method == 'POST' and form.validate():
        room = rooms_fabric.create(form.data) if form.data.get('title', None) else rooms_fabric.create(
            {'title': default_room_title, "is_private": False})
        if room:
            current_user.add_room(room)
            return Response(json.dumps(dict(success=True, room=room.id)), content_type='application/json')

    room_ids = [room.id for room in current_user.rooms]
    messages_data = dict()
    offset = int(request.cookies['offset']) / (-60) if request.cookies.get('offset') else 0
    for room_id in room_ids:
        messages_data[room_id] = rooms_fabric.get_messages_by_room(room_id)
    return render_template('room.html', rooms=rooms, form=form, messages_data=messages_data,
                           room_ids=room_ids, offset=datetime.timedelta(hours=offset))


@rooms.route('/join/room/<room_id>')
@login_required
def room_join(room_id):
    room = rooms_fabric.get_by_id(room_id)
    if room not in current_user.rooms:
        current_user.add_room(room)
    return redirect('/rooms#%s' % room.id)


@rooms.route("/rooms/invite/email", methods=['GET', 'POST'])
def send_invite_email():
    form = EmailRoomForm(request.form)
    if request.method == 'POST' and form.validate():
        link = 'http://' + request.__dict__['environ']['HTTP_HOST'] + '/join/room/%s' % form.data.get('room', None)
        # email_handler.send_email("Join room at MiMiChat!", ADMINS[0], [form.data.get('email', None)],
        #                          html_text=render_template('invite_to_room.html', url=link,
        #                                                    username=current_user.username))
        send_email.delay("Join room at MiMiChat!", ADMINS[0], form.data.get('email', None),
                         html_text=render_template('invite_to_room.html', url=link,
                                                   username=current_user.username))
        return Response(json.dumps(dict(success=True)), content_type='application/json')
    return Response(json.dumps(dict(success=False, errors=form.errors)), content_type='application/json')
