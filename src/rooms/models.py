from src.base.BaseModel import BaseModel
from src.flask_app import db


class Room(BaseModel):
    __tablename__ = 'rooms'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    is_private = db.Column(db.Boolean, default=False)


db.create_all()
db.session.commit()