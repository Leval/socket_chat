from src.base.BaseFabric import BaseFabric
from src.rooms.models import Room


class RoomsFabric(BaseFabric):
    model_handler = Room

    def get_messages_by_room(self, room_id):
        room = self.get_by_id(room_id)
        return room.messages

    def get_public_rooms(self):
        return self.get_by_filters(dict(is_private=False))