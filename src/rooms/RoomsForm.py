from wtforms import Form, TextField, IntegerField, BooleanField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Required


class CreateRoomForm(Form):
    title = TextField('title')
    is_private = BooleanField('is_private')


class EmailRoomForm(Form):
    email = EmailField('email', validators=[Required(message="Email is required")])
    room = IntegerField('room', validators=[Required(message="Room number is required")])