from flask.ext.login import LoginManager, AnonymousUserMixin

from src.flask_app import app
from src.users import index
from src.users.models import User

login_manager = LoginManager()
login_manager.init_app(app)
from src.users import users
from src.rooms import rooms
from src.messages import messages
from src.emails import emails

app.register_blueprint(users)
app.register_blueprint(rooms)
app.register_blueprint(messages)
app.register_blueprint(emails)


# @app.before_request
# def csrf_protect():
#     if request.method == "POST":
#         token = session.pop('_csrf_token', None)
#         if not token or token != request.form.get('_csrf_token'):
#             abort(403)
#
# def generate_csrf_token():
#     if '_csrf_token' not in session:
#         session['_csrf_token'] = some_random_string()
#     return session['_csrf_token']
#
# app.jinja_env.globals['csrf_token'] = generate_csrf_token

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


class Anonymous(AnonymousUserMixin):
    def __init__(self):
        self.username = 'Guest'

login_manager.anonymous_user = Anonymous
login_manager.unauthorized_handler(index)

if __name__ == '__main__':
    app.run()
