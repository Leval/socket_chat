import json
import logging
import sys
import re
import urllib2
from bs4 import BeautifulSoup
import datetime
import requests
from tornado import websocket
from tornado.httpclient import AsyncHTTPClient
import tornado.ioloop
import tornado.web
from tornado.options import define, options, parse_command_line
from src.messages import MessagesFabric, MessageForm
from src.rooms import RoomsFabric
from src.users import UserFabric

define("port", default=9000, help="run on the given port", type=int)

rooms = dict()
user_fabric = UserFabric()
messages_fabric = MessagesFabric()
rooms_fabric = RoomsFabric()
TEMPLATE = re.compile(r"(?:^|\s)(http[s]?:[^\s| ^<]+|www\.[^\s| ^<]+)")
# TEMPLATE = re.compile(r"(?:http[s]?:[^\s| ^<]+|www\.[^\s| ^<]+)")
SMILE1_1 = re.compile(r"\(devil\)")
SMILE1_2 = re.compile(r"\(dream\)")
SMILE1_3 = re.compile(r"([\n|\s]:'[-]?\()|(^:'[-]?\()")
SMILE1_4 = re.compile(r"([\n|\s]:[-]?\)\))|(^:[-]?\)\))")
SMILE1_5 = re.compile(r"([\n|\s]:D)|(^:D)")
SMILE1_6 = re.compile(r"\(watch\)")
SMILE2_1 = re.compile(r"\(cool\)")
SMILE2_2 = re.compile(r"([\n|\s]:[-]?\))|(^:[-]?\))")
SMILE2_3 = re.compile(r"([\n|\s]:[-]?\()|(^:[-]?\()")
SMILE2_4 = re.compile(r"([\n|\s]:[-]?\|)|(^:[-]?\|)")
SMILE2_5 = re.compile(r"([\n|\s]:[-]?\$)|(^:[-]?\$)")
SMILE2_6 = re.compile(r"\(beaten\)")
# SMILE3_1 = re.compile(r"\}:[-]?>")
SMILE3_2 = re.compile(r"\(angry\)")
SMILE3_3 = re.compile(r"([\n|\s]8[-]?\|)|(^8[-]?\|)")
SMILE3_4 = re.compile(r"\(surprised\)")
SMILE3_5 = re.compile(r"([\n|\s]:[-]?O)|(^:[-]?O)")
SMILE3_6 = re.compile(r"\(rolf\)")
SMILE4_1 = re.compile(r"([\n|\s]:[-]?/)|(^:[-]?/)") # :/
SMILE4_2 = re.compile(r"\(smile\)")
SMILE4_3 = re.compile(r"([\n|\s]:[-]?\[)|(^:[-]?\[)") # :[


class WebSocketHandler(websocket.WebSocketHandler):
    def open(self, *args):
        self.stream.set_nodelay(True)
        self.room_ids = list()
        self.id = None
        self.username = None

    def on_message(self, message):
        message_data = json.loads(message)
        text = ''
        if message_data.get('type', None) == "message":
            try:
                post_data = dict(user_id=message_data.get('id', None), room_id=message_data.get('room_id', None),
                                 text=message_data.get('text', None))
                form = MessageForm(data=post_data)
                if form.validate():
                    room = rooms_fabric.get_by_id(form.data.get('room_id'))
                    user = user_fabric.get_by_id(form.data.get('user_id'))
                    post_data['room'] = room
                    post_data['user'] = user
                    post_data['created_date'] = datetime.datetime.utcnow()
                    post_data['text'] = text = self.parse_links(post_data.get('text'))
                    messages_fabric.create(post_data)
            except Exception as e:
                logging.error("Message doesn't saved: %r" % e)
            if message_data.get('room_id', None):
                for item in rooms[int(message_data.get('room_id', None))]['clients']:
                    if message_data.get('text', None) != "":
                        hours = float(message_data.get('tzoffset', None)) / -60
                        item['object'].write_message(json.dumps(dict(type="message",
                                                                     room=message_data.get('room_id', None),
                                                                     username=message_data.get('username'),
                                                                     datetime=(datetime.datetime.utcnow() +
                                                                               datetime.timedelta(hours=hours)).strftime('%d-%m-%Y %H:%M'),
                                                                     text=text)))

        elif message_data.get('type', None) == "rooms":
            self.room_ids = message_data.get('text', None)
            self.id = message_data.get('id', None)
            self.username = message_data.get('username', None)

            for room_id in message_data.get('text', None):
                if not rooms.get(room_id, None):
                    rooms[room_id] = {'room_id': room_id, 'clients': []}
                self.item = {"id": message_data.get('id', None),
                             "username": message_data.get('username', None),
                             "object": self}
                rooms[room_id]['clients'].append(self.item)
            self.write_message(json.dumps(dict(type='rooms',
                                               text='Connected...')))
        elif message_data.get('type', None) == "members":
            for room_id in self.room_ids:
                if self.item in rooms[room_id]['clients']:
                    members = []
                    members_ids = []
                    for user in rooms[room_id]['clients']:
                        members_ids.append(user.get('id'))
                    members_ids = set(members_ids)
                    for user in rooms[room_id]['clients']:
                        if user.get('id') in members_ids:
                            members.append(dict(id=user.get('id'), username=user.get('username')))
                            members_ids.remove(user.get('id'))
                    for item in rooms[room_id]['clients']:
                        item['object'].write_message(json.dumps(dict(type='members',
                                                                     room=room_id,
                                                                     members=members)))
        elif message_data.get('type', None) == 'typing':
            if message_data.get('room', None):
                room_id = int(message_data.get('room', None))
                for item in rooms[room_id]['clients']:
                    item['object'].write_message(json.dumps(dict(type='typing',
                                                                 room=room_id, id=self.id)))
        elif message_data.get('type', None) == 'ping':
            self.write_message(json.dumps(dict(type='pong')))

    def parse_links(self, text):
        text = text.replace("&nbsp;", " ")
        text = SMILE1_1.sub('<div class="smile-1-1"></div>', text)
        text = SMILE1_2.sub('<div class="smile-1-2"></div>', text)
        text = SMILE1_3.sub('<div class="smile-1-3"></div>', text)
        text = SMILE1_4.sub('<div class="smile-1-4"></div>', text)
        text = SMILE1_5.sub('<div class="smile-1-5"></div>', text)
        text = SMILE1_6.sub('<div class="smile-1-6"></div>', text)
        text = SMILE2_1.sub('<div class="smile-2-1"></div>', text)
        text = SMILE2_2.sub('<div class="smile-2-2"></div>', text)
        text = SMILE2_3.sub('<div class="smile-2-3"></div>', text)
        text = SMILE2_4.sub('<div class="smile-2-4"></div>', text)
        text = SMILE2_5.sub('<div class="smile-2-5"></div>', text)
        text = SMILE2_6.sub('<div class="smile-2-6"></div>', text)
        # text = SMILE3_1.sub('<div class="smile-3-1"></div>', text)
        text = SMILE3_2.sub('<div class="smile-3-2"></div>', text)
        text = SMILE3_3.sub('<div class="smile-3-3"></div>', text)
        text = SMILE3_4.sub('<div class="smile-3-4"></div>', text)
        text = SMILE3_5.sub('<div class="smile-3-5"></div>', text)
        text = SMILE3_6.sub('<div class="smile-3-6"></div>', text)
        text = SMILE4_1.sub('<div class="smile-4-1"></div>', text)
        text = SMILE4_2.sub('<div class="smile-4-2"></div>', text)
        text = SMILE4_3.sub('<div class="smile-4-3"></div>', text)

        links = set(TEMPLATE.findall(text))
        for link in links:
            if link.startswith("www"):
                link = "http://" + link
            # http_client = AsyncHTTPClient()
            # response = http_client.fetch(link, self.handle_asyncrequest)
            r = requests.get(link, stream=True)
            if ('image' in r.headers.get('content-type') or 'jpg' in r.headers.get('content-type') or
                        'gif' in r.headers.get('content-type') or 'jpeg' in r.headers.get('content-type') or
                        'pjpeg' in r.headers.get('content-type') or 'png' in r.headers.get('content-type')):

                text = text.replace(link, '<a href="%s" target="_blank">%s</a>' % (link, link))
                text += '<p><img src="%s"/></p>' % link
            else:
                try:
                    res = requests.get(link)
                    soup = BeautifulSoup(res.content)
                    title = soup.find('title').text
                    text = text.replace(link, '<a href="%s" target="_blank">%s</a>' % (link, title))

                    image = soup.find(rel='image_src')
                    image_url = image.attrs.get('href') if image else '/rooms/static/img/paws.jpg'
                    description = soup.find("meta", attrs={"name": "description"})
                    description_text = description.attrs.get('content') if description else ''
                    text += '''
                            <div class="link-preview">
                                <div class="link-preview-image"><img class="preview-img" src="%s"/></div>
                                <div class="link-preview-content">
                                    <span class="link-preview-title">%s</span><br/>%s
                                </div>
                                <a href="%s" target="_blank"></a>
                            </div>
                            ''' % (image_url, title, description_text, link)
                except Exception as e:
                    logging.error("Error while parsing link: %r" % e)
        return text

    def handle_asyncrequest(self, response):
        print response.request.__dict__

    def on_close(self):
        for room_id in self.room_ids:
            if self.item in rooms[room_id]['clients']:
                rooms[room_id]['clients'].remove(self.item)
            members = []
            for user in rooms[room_id]['clients']:
                members.append(dict(id=user.get('id'), username=user.get('username')))
            for item in rooms[room_id]['clients']:
                item['object'].write_message(json.dumps(dict(type='members',
                                                             room=room_id,
                                                             text='%s disconnected' % self.username,
                                                             members=members)))

    def check_origin(self, origin):
        return True


# tr = WSGIContainer(flask_app)
app = tornado.web.Application([
    (r'/socket/', WebSocketHandler),
    # (r".*", tornado.web.FallbackHandler, dict(fallback=tr)),
])

if __name__ == '__main__':
    parse_command_line()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
