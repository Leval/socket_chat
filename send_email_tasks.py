# celery -A tasks worker --loglevel=info
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from celery import Celery
from src.config import Config

BROKER_URL = 'redis://localhost:6379/3'
# redis://:password@hostname:port/db_number
capp = Celery('email', broker=BROKER_URL)

email_server = Config().server


@capp.task
def send_email(subject, sender, recipients, body_text='', html_text=''):
    try:
        msg = MIMEMultipart()
        msg['From'] = sender
        msg['To'] = recipients
        msg['Subject'] = subject
        msg.attach(MIMEText(body_text, 'plain'))
        msg.attach(MIMEText(html_text, 'html'))

        text = msg.as_string()
        email_server.sendmail(sender, recipients, text)
        return "Success"
    except Exception:
        return "Failure"